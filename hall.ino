#include <WiFi.h>
#include <HTTPClient.h>


int analogPin = A12;
int val = 0;
int digitalPin = 14;
bool detect = false;
unsigned long myTime = 0;
unsigned long roundTime;
const char* ssid = "gyro";
const char* password = "1234567899";
HTTPClient http;
String serverPath = "http://192.168.4.1/?rpm=";


void initWiFi() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.print("Connecting to WiFi ..");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print('.');
    delay(1000);
  }
  Serial.println(WiFi.localIP());
}

void setup() {
  pinMode(digitalPin, INPUT);
  Serial.begin(115200);

  initWiFi();
}

void loop() {
  int analogVal = analogRead(analogPin);
  int digitalVal = digitalRead(digitalPin);
  //Serial.print(analogVal);
  //Serial.print("\t");
  if (detect == false && digitalVal == 0) {
    
    if (myTime > 0) {
      roundTime = micros() - myTime;
      String request = serverPath;
      double rps = 1000000.0 / roundTime;
      request.concat(rps);

      //Serial.println(roundTime);
      //Serial.println(request);
      
      http.begin(request.c_str());
      
      // Send HTTP GET request
      int httpResponseCode = http.GET();
      
      if (httpResponseCode>0) {
        Serial.print("HTTP Response code: ");
        Serial.println(httpResponseCode);
        //String payload = http.getString();
        //Serial.println(payload);
        Serial.println(request);
      }
      else {
        Serial.print("Error code: ");
        Serial.println(httpResponseCode);
        Serial.println(rps);
      }

      http.end();
    }
    
    myTime = micros();
    detect = true;
    //Serial.println("YES");
  } else if (detect == true && digitalVal == 1) {
    detect = false;
    //Serial.println("NO");
  }
  //Serial.println(digitalVal);
  //val = hallRead();
  // print the results to the serial monitor
  //Serial.println(val); 
  delay(10);
}
